package com.example.catolica2017.cadastroemtxt;

import android.util.Log;

/**
 * Created by catolica2017 on 26/10/17.
 */

public class Carro {
    int id;
    String nome;
    String desc;
    String url_info;
    String url_foto;
    String url_video;
    String latitude;
    String longitude;

    public Carro(int id, String nome, String desc, String url_info, String url_foto, String url_video, String latitude, String longitude) {
        this.id = id;
        this.nome = nome;
        this.desc = desc;
        this.url_info = url_info;
        this.url_foto = url_foto;
        this.url_video = url_video;
        this.latitude = latitude;
        this.longitude = longitude;
        Log.i("infor", url_video);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getUrl_info() {
        return url_info;
    }

    public void setUrl_info(String url_info) {
        this.url_info = url_info;
    }

    public String getUrl_foto() {
        return url_foto;
    }

    public void setUrl_foto(String url_foto) {
        this.url_foto = url_foto;
    }

    public String getUrl_video() {
        return url_video;
    }

    public void setUrl_video(String url_video) {
        this.url_video = url_video;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
