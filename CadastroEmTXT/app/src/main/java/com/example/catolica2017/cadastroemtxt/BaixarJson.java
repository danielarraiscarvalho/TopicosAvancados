package com.example.catolica2017.cadastroemtxt;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



/**
 * Created by catolica2017 on 26/10/17.
 */

@SuppressWarnings("WrongThread")
@RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
class BaixarJson extends AsyncTask<URL, Void, ArrayList>{

    //Metodo que executa a thread em segundo plano
    public ArrayList<Carro> doInBackground(URL...urls){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(urls[0]).build();
        ArrayList<Carro> carroArrayList;
        try {
            Response response = client.newCall(request).execute();
            String json = response.body().string();
            JSONArray minhaArray = new JSONArray(json);
            carroArrayList = new ArrayList<>();
            for (int i = 0; i < minhaArray.length(); i++) {
                JSONObject jsonObject = new JSONObject(minhaArray.getString(i));
                Carro carro = new Carro(jsonObject.getInt("id"),
                        jsonObject.getString("nome").replace("\n",""),
                        jsonObject.getString("desc").replace("\n",""),
                        jsonObject.getString("url_info").replace("\n",""),
                        jsonObject.getString("url_foto").replace("\n",""),
                        jsonObject.getString("url_video").replace("\n",""),
                        jsonObject.getString("latitude").replace("\n",""),
                        jsonObject.getString("longitude").replace("\n",""));
                carroArrayList.add(carro);
            }
            return carroArrayList;
        } catch (Exception e){

        }
        return null;
    }

    //Metodo que executa após conclusao da thread
    //Codigo chamado ja na thread principal
    public void onPostExecute(ArrayList<Carro> carros){

    }
}


