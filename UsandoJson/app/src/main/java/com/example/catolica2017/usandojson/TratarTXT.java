package com.example.catolica2017.usandojson;

import android.app.Activity;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by catolica2017 on 21/09/17.
 */

public class TratarTXT {

    Activity atividade;

    public TratarTXT(Activity atividade) {
        this.atividade = atividade;
    }

    public void gravarTXT(String dado) throws IOException {
        FileWriter arq = new FileWriter(atividade.getFilesDir().getPath()+"/dados.txt", true);
        PrintWriter gravarArq = new PrintWriter(arq);

        arq.write(dado+"\n");

        arq.flush();
        arq.close();
    }

    public void imprimirNoLog() throws IOException {
        FileReader arq = new FileReader(atividade.getFilesDir().getPath()+"/dados.txt");
        BufferedReader buffer = new BufferedReader(arq);

        String linha = null;

        while ((linha = buffer.readLine()) != null){
            Log.i("infor", linha);
        }
    }

    public void apagarDados() throws IOException {
        Writer clean = new BufferedWriter(new FileWriter(atividade.getFilesDir().getPath()+"/dados.txt"));
        clean.close();
    }


    public ArrayList<Pessoa> retornarPessoas() throws IOException {
        FileReader arq = new FileReader(atividade.getFilesDir().getPath()+"/dados.txt");
        ArrayList<Pessoa> pessoas = new ArrayList<>();
        BufferedReader buffer = new BufferedReader(arq);

        String linha = null;

        while ((linha = buffer.readLine()) != null){
            String[] dados = linha.split(";");
            pessoas.add(new Pessoa(dados[0],dados[1], dados[2]));
        }
        return pessoas;
    }
}
