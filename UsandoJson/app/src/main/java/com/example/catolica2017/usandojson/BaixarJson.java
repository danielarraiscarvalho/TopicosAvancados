package com.example.catolica2017.usandojson;


import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;



/**
 * Created by catolica2017 on 26/10/17.
 */

@SuppressWarnings("WrongThread")
@RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
class BaixarJson extends AsyncTask<URL, Void, ArrayList>{

    //Metodo que executa a thread em segundo plano
    public ArrayList<Carro> doInBackground(URL...urls){
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(urls[0]).build();
        try {
            Response response = client.newCall(request).execute();
            String json = response.body().string();
            TrataJson trataJson = new TrataJson();
            return trataJson.carroArrayList(json);
        } catch (Exception e){

        }
        return null;
    }

    //Metodo que executa após conclusao da thread
    //Codigo chamado ja na thread principal
    public void onPostExecute(ArrayList<Carro> carros){

    }
}


