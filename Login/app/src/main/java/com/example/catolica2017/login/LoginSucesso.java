package com.example.catolica2017.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.DecimalFormat;

public class LoginSucesso extends AppCompatActivity {

    TextView senha;
    TextView email;

    String e;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_sucesso);
        senha = (TextView) findViewById(R.id.lbSenha);
        email = (TextView) findViewById(R.id.lbNome);

        e = getIntent().getExtras().getString("email");
        s = getIntent().getExtras().getString("senha");
        //senha.setText(s);
        email.setText(e);
        SharedPreferences lgShared = getSharedPreferences("login",MODE_PRIVATE);

        String senhab = lgShared.getString("senha","Erro!");
        senha.setText(senhab);
    }

    public void lougout(View view){
        finish();
    }

    @Override
    public void finish() {
        Intent resultado = new Intent();
        Bundle dados  = new Bundle();
        dados.putString("email", e);
        resultado.putExtras(dados);
        setResult(1, resultado);
        super.finish();
    }


    public void recuperaLogin(){

    }
}
