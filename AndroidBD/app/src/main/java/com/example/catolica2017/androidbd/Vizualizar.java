package com.example.catolica2017.androidbd;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class Vizualizar extends AppCompatActivity {
    ListView listView;
    int itemClicado;
    Carro carro;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vizualizar);

        listView = (ListView) findViewById(R.id.lvCarro);
        ContentValues contentValues = new ContentValues();

        ClassBD classBD = new ClassBD(getBaseContext(), "carro", 1);

        BaseAdapter baseAdapter = null;
        baseAdapter = new CarroAdapter(classBD.listaCarro(), this);

        listView.setAdapter(baseAdapter);
    }

}
