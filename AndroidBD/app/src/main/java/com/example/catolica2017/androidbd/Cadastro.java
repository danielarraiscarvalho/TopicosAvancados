package com.example.catolica2017.androidbd;

import android.content.ContentValues;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class Cadastro extends AppCompatActivity {

    EditText etNome = null;
    EditText etPlaca = null;
    EditText etAno = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        etNome = (EditText) findViewById(R.id.etNome);
        etPlaca = (EditText) findViewById(R.id.etPlaca);
        etAno = (EditText) findViewById(R.id.etAno);

    }

    public void Cadastrar(View view){
        ContentValues contentValues = new ContentValues();
        contentValues.put("nome", etNome.getText().toString());
        contentValues.put("placa", etPlaca.getText().toString());
        contentValues.put("ano", etAno.getText().toString());
        ClassBD classBD = new ClassBD(getBaseContext(), "carro", 1);
        classBD.insereCarro(contentValues);
    }
}
