package com.example.catolica2017.androidbd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Inicial extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicial);
    }

    public void telaCadastro(View view){
        Intent verCadastros = new Intent(this, Cadastro.class);
        startActivity(verCadastros);
    }

    public void telaVizualizaCadastros(View view){
        Intent verCadastros = new Intent(this, Vizualizar.class);
        startActivity(verCadastros);
    }
}
