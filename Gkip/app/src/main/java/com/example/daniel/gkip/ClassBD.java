package com.example.daniel.gkip;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.daniel.gkip.Nota;

import java.util.ArrayList;

public class ClassBD extends SQLiteOpenHelper {
    public final String apagaBancoSQL = "drop database if exists notas";
    String[] scripCriaBanco = {"CREATE TABLE  nota ( id_nota INTEGER PRIMARY KEY AUTOINCREMENT, nota TEXT NOT NULL, titulo TEXT NOT NULL, data TEXT NOT NULL, arquivado INTEGER NOT NULL DEFAULT (0));"};

    Context context;

    //Contrutor da classe
    //nele deve ser passado o contexto e a versão do banco
    public ClassBD(Context context, String name, int version) {
        super(context, name + ".db", null, version);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (int i = 0; i < scripCriaBanco.length; i++) {
            db.execSQL(scripCriaBanco[i]);
        }
    }

    //Método que insere uma nota no banco
    //Ele tem como retorno, em formato long, o ID do objeto que foi inserido
    public long insereNota(ContentValues values) {
        SQLiteDatabase database = this.getWritableDatabase();
        long id = database.insert("nota", null, values);
        Toast.makeText(context, "Nota inserida", Toast.LENGTH_SHORT).show();
        return id;
    }

    //Método que atualiza uma nota.
    //Devem os valores a serem atualizados devem ser passados em forma de um Map
    //Para o métod saber qual nota atualizar, deve-se passa como paramêtro o ID
    public void updateNota(ContentValues nota, int idNota) {
        SQLiteDatabase database = this.getWritableDatabase();
        database.update("nota", nota, "id_nota=" + idNota, null);
        Toast.makeText(context, "Nota atualizada", Toast.LENGTH_SHORT).show();
    }

    //Método que retorna uma nota cadastrada no banco
    //de acordo com o id passado como paramêtro
    public Nota buscarNota(int id) {
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.rawQuery("select * from nota where id_nota =" + id, null);
        Nota nota = null;
        if (cursor.moveToFirst()) {
            nota = new Nota(cursor.getInt(0), cursor.getString(2), cursor.getString(1), cursor.getString(3), cursor.getInt(4)==1?true:false);
        }
        return nota;
    }

    //Método que remove uma nota do Banco
    public void removerNota(int id) {
        SQLiteDatabase database = this.getWritableDatabase();
        int deletou = database.delete("nota", "id_nota =" + id, null);

        //Verifica se o houve a deleção do objeto
        if (deletou > 0) {
            Toast.makeText(context, "Nota removida", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(context, "Nota não removida", Toast.LENGTH_SHORT).show();
        }
    }

    //Método que retorna todoas as notas dacastradas no banco
    public ArrayList<Nota> listaNotas() {

        ArrayList<Nota> carros = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query("nota", new String[]{"id_nota", "nota", "titulo", "data", "arquivado"}, "arquivado = ?", new String[]{"0"}, null, null, null);

        while (cursor.moveToNext()) {
            carros.add(new Nota(cursor.getInt(0), cursor.getString(2), cursor.getString(1), cursor.getString(3), cursor.getInt(4)==1?true:false));
        }

        return carros;
    }

    public ArrayList<Nota> listaNotasArquivadas() {

        ArrayList<Nota> carros = new ArrayList<>();

        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query("nota", new String[]{"id_nota", "nota", "titulo", "data", "arquivado"}, "arquivado = ?", new String[]{"1"},null, null, null);

        while (cursor.moveToNext()) {
            carros.add(new Nota(cursor.getInt(0), cursor.getString(2), cursor.getString(1), cursor.getString(3), cursor.getInt(4)==1?true:false));
        }

        return carros;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
