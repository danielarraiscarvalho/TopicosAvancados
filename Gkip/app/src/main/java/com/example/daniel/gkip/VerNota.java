package com.example.daniel.gkip;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class VerNota extends AppCompatActivity {
    String id= null;
    TextView tvTitulo = null;
    TextView tvNota = null;
    Button arquivar;
    Nota nota;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_nota);

        //Criação de instâncias dos TextViews
        tvTitulo = (TextView) findViewById(R.id.tvVTitulo);
        tvNota = (TextView) findViewById(R.id.tvVNota);

        arquivar = (Button) findViewById(R.id.btArquivar);

        id = getIntent().getExtras().getString("id");

        atualizarTela();
    }

    //Método que abre tela de edição, e envia o ID da nota a ser editada
    //A tela de edição é a mesma de criação
    public void editar(View view){
        Intent intent = new Intent(this, CriarNota.class);
        Bundle b = new Bundle();
        b.putString("id", String.valueOf(id));
        intent.putExtras(b);
        startActivity(intent);
    }

    //Método de ação do botão excluir
    public void excluir(View view){
        ClassBD classBD = new ClassBD(this, "notas", 1);
        classBD.removerNota(nota.getId());
        super.finish();
    }

    //Método de ação do botão excluir
    public void arquivar(View view){
        ClassBD classBD = new ClassBD(this, "notas", 1);
        ContentValues contentValues = new ContentValues();
        //Se a nota estiver arquivada ela passa a ser desarquivada  e vice versa
        contentValues.put("arquivado", nota.isArquivado()?0:1);
        classBD.updateNota(contentValues, nota.getId());
        atualizarTela();
    }

    @Override
    //Toda vez, antes da tela ganhar foco novamente, as informações da nota
    //são buscadas no banco e atualizadas na atividade
    protected void onResume() {
        ClassBD classBD = new ClassBD(this, "notas", 1);
        nota = classBD.buscarNota(Integer.valueOf(id));
        tvTitulo.setText(nota.getTitulo());
        tvNota.setText(nota.getNota());
        super.onResume();
    }

    public void atualizarTela(){
        ClassBD classBD = new ClassBD(this, "notas", 1);

        nota = classBD.buscarNota(Integer.valueOf(id));

        //Verifica se a nota está arquivada e ltera o texto do botão arquivar
        if (nota.isArquivado()){
            arquivar.setText("Desarquivar nota");
        }else{
            arquivar.setText("Arquivar nota");
        }

        tvTitulo.setText(nota.getTitulo());
        tvNota.setText(nota.getNota());
    }

    public void voltar(View view){
        super.finish();
    }
}
