package com.example.daniel.gkip;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by daniel on 02/12/17.
 */

public class NotaAdapter extends BaseAdapter {

    public ArrayList<Nota> notas;
    private Context context;

    public NotaAdapter(ArrayList<Nota> notas, Context contexto) {
        this.notas = notas;
        this.context = contexto;
    }

    @Override
    public int getCount() {
        return notas.size();
    }

    @Override
    public Object getItem(int position) {
        return notas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View reciclado, ViewGroup parent) {
        View layout = null;
        if (reciclado==null){
            layout = LayoutInflater.from(context).inflate(R.layout.activity_celula, parent, false);
        }else{
            layout = reciclado;
        }

        final Nota nota = (Nota) getItem(position);

        TextView titulo = layout.findViewById(R.id.tvTitulo);
        TextView data = layout.findViewById(R.id.tvData);
        titulo.setText(nota.getTitulo());

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        data.setText(sdf.format(new Date(Long.parseLong(nota.getDate()))));

        return layout;
    }


}
